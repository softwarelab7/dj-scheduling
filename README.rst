=============================
Django Scheduling
=============================

.. image:: https://badge.fury.io/py/dj-scheduling.png
    :target: https://badge.fury.io/py/dj-scheduling

.. image:: https://travis-ci.org/aidzz/dj-scheduling.png?branch=master
    :target: https://travis-ci.org/aidzz/dj-scheduling

Your project description goes here

Documentation
-------------

The full documentation is at https://dj-scheduling.readthedocs.org.

Quickstart
----------

Install Django Scheduling::

    pip install dj-scheduling

Then use it in a project::

    import dj_scheduling

Features
--------

* TODO

Running Tests
--------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install -r requirements_test.txt
    (myenv) $ python runtests.py

Credits
---------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage

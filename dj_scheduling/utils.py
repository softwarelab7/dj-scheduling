import datetime


class Schedule(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def __repr__(self):
        return u"start: {} end: {}".format(self.start, self.end)


def get_schedule_blocks(queryset, start, end, padded=False, padding=0):
    """
    if padded=True, use padded_end and padded_start
    """
    schedules = []
    for item in queryset:
        if padded:
            item_start = item.padded_start - datetime.timedelta(minutes=padding)
            item_end = item.padded_end + datetime.timedelta(minutes=padding)
        else:
            item_start = item.start - datetime.timedelta(minutes=padding)
            item_end = item.end + datetime.timedelta(minutes=padding)
        schedule = Schedule(start=None, end=None)
        if item_start < start:
            schedule.start = start
        else:
            schedule.start = item_start
        if item_end > end:
            schedule.end = end
        else:
            schedule.end = item_end
        schedules.append(schedule)
    return schedules


def get_merged_schedules(schedules, duration):
    """
    Merge schedules whose start and end overlap the specified duration
    """
    schedules.sort(key=lambda x: (x.start, x.end))
    current = None
    merged = []
    for schedule in schedules:
        sched = Schedule(start=None, end=None)
        if not current:
            merged.append(schedule)
            current = schedule
        elif (schedule.end > current.end):
            if current and (current.end - schedule.start).total_seconds() / 60 >= duration:
                merged.pop()
                sched.start = current.start
                sched.end = schedule.end
                merged.append(sched)
                current = sched
            else:
                merged.append(schedule)
                current = schedule
    return merged


def get_object_minutes(obj, default):
    """
    function to return either obj.minutes if it exists, or default.
    used with AppointmentPadding and AvailableSchedulesInterval objects
    """
    if obj:
        if obj.minutes is not None:
            minutes = obj.minutes
        else:
            minutes = default
    else:
        minutes = default
    return minutes


def get_padding(obj, default):
    """
    need to retain this while changes not yet merged in. delete eventually
    """
    if obj:
        if obj.minutes is not None:
            minutes = obj.minutes
        else:
            minutes = default
    else:
        minutes = default
    return minutes


def pad_startdt_and_enddt(start, end, padding):
    # add padding to start and end
    padded_start = start - datetime.timedelta(minutes=padding)
    padded_end = end + datetime.timedelta(minutes=padding)
    return padded_start, padded_end


def round_startdt_enddt_to_nearest(start, end, minute_interval):
    """
    round start to nearest later minute_interval
    (ex. 3:03 to 3:05 if minute_interval is 5)
    round end to nearest earlier minute_interval
    (ex. 3:03 to 3:00 of minute_interval is 5)
    """
    if start.minute % minute_interval > 0:
        start = start + datetime.timedelta(minutes=(minute_interval - start.minute % minute_interval))
    if end.minute % minute_interval > 0:
        end = end - datetime.timedelta(minutes=end.minute % minute_interval)
    # strip seconds and microseconds
    start = start.replace(second=0, microsecond=0)
    end = end.replace(second=0, microsecond=0)
    return start, end


def datetime_range(start, end, delta):
    """
    returns range of datetime with interval delta from start to end
    includes start and end
    """
    current = start
    if not isinstance(delta, datetime.timedelta):
        delta = datetime.timedelta(**delta)
    while current <= end:
        yield current
        current += delta

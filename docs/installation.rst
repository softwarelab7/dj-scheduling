============
Installation
============

At the command line::

    $ easy_install dj-scheduling

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv dj-scheduling
    $ pip install dj-scheduling
